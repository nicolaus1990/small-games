
from tkinter import *
from tkinter import ttk
import time

from minesweeperDatastructure import Pos, Tile

#current_milli_time taken from: Naftuli Kay
#from https://stackoverflow.com/questions/5998245/get-current-time-in-milliseconds-in-python
current_milli_time = lambda: int(round(time.time() * 1000))

class GUI(object):

    def __init__(self,dimX,dimY,fn, tileSize=30):
        self.dimX=dimX
        self.dimY=dimY
        self.notifyControllerFn = fn
        self.tileSize = tileSize
        # Main window:
        self.root = Tk()
        self.root.title("Minesweeper in Python")
        #root.geometry("900x600+300+300")
        self.canvas = Canvas(self.root)
        #canvas.grid(column=self.dimX,row=self.dimY) 
        self.canvas.grid(column=0,row=0)
        self.drawMinefield()
        self.addEventhandling()

    def startGame(self):
        self.root.mainloop()

    def addEventhandling(self):
        self.canvas.bind("<B1-ButtonRelease>", self.dispatch)
        #self.canvas.bind("<Button-1>", self.dispatch)
        
    def drawMinefield(self):#buttonFns?
        #squareIds to add to every button a fn?
        for i in range(self.dimX):
            for j in range(self.dimY):
                self.canvas.create_rectangle((i * self.tileSize, \
                                              j * self.tileSize, \
                                              i * self.tileSize + self.tileSize,\
                                              j * self.tileSize +self.tileSize), \
                                             fill="grey")


    def drawSquare(self, pos, color="grey"):
        x = pos.getX()
        y = pos.getY()
        self.canvas.create_rectangle((x * self.tileSize,\
                                 y * self.tileSize,\
                                (x+1) * self.tileSize,\
                                (y+1) * self.tileSize),\
                                fill=color)

    def dispatch(self,e):
        if self.clickedOnMinefield(e):
            pos = self.getPosition(e)
            self.notifyControllerFn(pos)

    def getPosition(self,e):
        return Pos(int(e.x /self.tileSize), \
                   int(e.y /self.tileSize))

    def clickedOnMinefield(self,e):
        return 0 <= e.x < self.dimX * self.tileSize and \
            0 <= e.y < self.dimY * self.tileSize

    def uncover(self,tile):
        num = tile.getNumAdjMines()
        if num == 0:
            self.drawSquare(tile.getPosition(), "white")
        elif num > 0:
            self.drawSquare(tile.getPosition(), "white")
            l = Label(self.canvas, text=str(num), bg='white')
            l.place(x=tile.getX() * self.tileSize + self.tileSize/4,\
                    y=tile.getY() * self.tileSize + self.tileSize/4)

    def gameOver(self,lost=True):
        if lost:
            self.root.destroy()
        else:
            l = Label(self.canvas, text="You won!",\
                      bg='white', fg='green') # width=50,
            l.config(font=("Courier", 44))
            l.place(x=self.dimX * self.tileSize / 4,\
                    y=self.dimY * self.tileSize / 4)
            
    # This is not going to work because of concurrency ('command returns
    # immediately', callback is called every delay seconds.
    # http://search.cpan.org/dist/Tk/pod/after.pod
    # def uncoverSlow(self,tile):
    #     def helper():
    #         self.uncover(tile)
    #         self.canvas.update()
    #     self.canvas.after(1000,helper)

    def uncoverSlow(self,tile):
        delay = 100
        t1 = current_milli_time()
        t2 = t1
        while t2 - t1 < delay:
            t2 = current_milli_time()
        self.uncover(tile)
        #GUI needs to be updated, else it will update after everything is
        #done (waiting for events again).
        self.canvas.update()
