
import random as rnd

class Pos(object):
    '''
    '''
    def __init__(self,x,y):
        self.xCoord = x
        self.yCoord = y
    def getX(self):
        return self.xCoord
    def getY(self):
        return self.yCoord
    def setX(self,x):
        self.xCoord = x
    def setY(self,y):
        self.yCoord = y
    def add(self,pos):
        return Pos(self.getX() + pos.getX(),\
                   self.getY() + pos.getY())
    def __eq__(self,pos):
        return self.getX() == pos.getX() and \
            self.getY() == pos.getY()
    def __hash__(self):
        return self.getX() * 10000 + self.getY()
    def __str__(self):
        return "x-Coord: " + str(self.getX()) + \
            " y-Coord: " + str(self.getY())
    # def __del__(self):#For debugging gc
    #   class_name = self.__class__.__name__
    #   print(class_name, "destroyed")

class Tile(Pos):
    '''
    '''
    def __init__(self,x,y,num):
        self.numAdjMines = num
        super().__init__(x,y)

    def __init__(self,pos,num):
        self.numAdjMines = num
        super().__init__(pos.getX(),pos.getY())
        
    def getNumAdjMines(self):
        return self.numAdjMines

    def getPosition(self):
        return Pos(self.getX(),self.getY())

    def __str__(self):
        s = super().__str__() + " Mines around: " + str(self.getNumAdjMines())
        return s

class Minefield(object):
    '''
    '''
    def __init__(self,dimX,dimY,numMines):
        self.dimX = dimX
        self.dimY = dimY
        self.numMines = numMines
        self.mines = set()
        while len(self.mines)!=numMines:
            xrnd = rnd.randrange(0,dimX)
            yrnd = rnd.randrange(0,dimY)
            self.mines.add(Pos(xrnd,yrnd))
        self.uncoveredTiles=set()

    def isMine(self,pos):
        return pos in self.mines

    def isCovered(self,pos):
        return not pos in self.uncoveredTiles

    def isValid(self,pos):
        return 0 <= pos.getX() < self.dimX and\
            0 <= pos.getY() < self.dimY

    def uncover(self,pos):
        assert self.isValid(pos) and not self.isMine(pos)
        self.uncoveredTiles.add(pos)

    def numAdjMines(self,pos):
        adjPoss = self.adjacentPositions(pos)
        return len(list(filter(self.isMine,adjPoss)))

    def adjacentPositions(self,pos):
        adjPoss = [pos.add(Pos(i,j)) for i in [-1, 0, 1] for j in [-1, 0, 1] \
                    if i!=0 or j!=0]
        return filter(self.isValid, adjPoss)

    def won(self):
        return len(self.uncoveredTiles) == self.dimX * self.dimY - self.numMines
    
    def __str__(self):
        strToPrint = " "
        for i in range(self.dimX+2): strToPrint+="-" 
        for j in range(self.dimY):
            line="\n| "
            for i in range(self.dimX):
                p = Pos(i,j)
                if self.isMine(p):
                    line+="X"
                else:
                    num = self.numAdjMines(p)
                    if num > 0:
                        line+=str(num)
                    else:# No bombs around (zero)
                        line+=" "
            strToPrint+=line + " | "
        strToPrint+="\n "
        for i in range(self.dimY+2): strToPrint+="-" 
        return strToPrint
