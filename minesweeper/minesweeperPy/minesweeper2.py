#!/usr/bin/python3

import sys

from queue import Queue, LifoQueue
import minesweeperGUI as ui
from minesweeperDatastructure import Pos, Tile, Minefield


class MinefieldController(object):
    '''
    '''
    def __init__(self,dimX,dimY,numMines):
        self.minefield = Minefield(dimX,dimY,numMines)
        print(self.minefield)
        self.ui = ui.GUI(dimX,dimY,self.tileClicked)
        
        
    def startGame(self):
        self.ui.startGame()

    def finishedGame(self):
        if self.minefield.won():
            self.ui.gameOver(False)

    def tileClicked(self, pos):
        if self.minefield.isMine(pos):
            self.ui.gameOver()
        elif self.minefield.isValid(pos) and \
             self.minefield.isCovered(pos):
            self.uncoverTiles(pos)
        else:
            pass
        

    def uncoverTiles(self,pos):
        '''
        Requires: Tile in pos is not a mine.
        '''
        def uncoverTile(tile):
            self.minefield.uncover(tile)
            self.ui.uncoverSlow(tile)#self.ui.uncover(tile)
        
        exploredTiles = set()
        furtherTiles = Queue(-1)#futher tiles to uncover
        #furtherTiles = LifoQueue(-1)#futher tiles to uncover
        furtherTiles.put(Tile(pos, self.minefield.numAdjMines(pos)))
        while not furtherTiles.empty():
            tile = furtherTiles.get()
            #print("Exploring: " + str(tile))
            exploredTiles.add(tile)
            uncoverTile(tile)
            if tile.getNumAdjMines() == 0:
                for p in self.minefield.adjacentPositions(tile):
                    #print("  Adding possibly further tile: " + str(p))
                    if not p in exploredTiles:#Note: Pos and Tile have same hash
                        num = self.minefield.numAdjMines(p)
                        #print("    tile: " + str(Tile(p,num)) + " unexplored.")
                        furtherTiles.put(Tile(p,num))
                    # else:
                    #     print("    Tile explored.")
        self.finishedGame()

            
                    
  
if __name__ == '__main__':
    # In terminal type:
    # python3 minesweeper.py
    
    # sys.argv[1] is the second argument in commandline
    # and should be the filename of the minefield
    #game = main(str(sys.argv[1]))

    #First test
    #m=Minefield(8,8,10)
    #m.printMinefield()

    m=MinefieldController(8,8,10)
    #m=MinefieldController(16,16,40)
    #m=MinefieldController(30,16,99)
    m.startGame()
    
