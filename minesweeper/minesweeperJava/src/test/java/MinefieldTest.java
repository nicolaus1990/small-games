import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Created by nikki on 2/27/18.
 */
public class MinefieldTest {

    private Minefield minef1;
    private int width1 = 8;
    private int height1 = 8;
    private int numMines1 = 10;
    List<Position> bombs1;

    private Minefield minef2;
    private int width2 = 31;
    private int height2 = 20;
    private int numMines2 = 43;
    List<Position> bombs2;

    @Before
    public void init() {
        minef1 = new Minefield(width1 , height1 , numMines1);
        bombs1 = minef1.getPosOfBombs();

        minef2 = new Minefield(width2 , height2 , numMines2);
        bombs2 = minef2.getPosOfBombs();
    }

    @Test
    public void isPosValid(){
        isPosValidHelper(minef1, height1, width1);
        isPosValidHelper(minef2, height2, width2);
    }

    private void isPosValidHelper(Minefield minef, int height, int width) {
        for (int i=0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                assertTrue(minef.isPosValid(i,j));
            }
        }
        assertFalse(minef.isPosValid(-1,0));
        assertFalse(minef.isPosValid(0,-1));
        assertFalse(minef.isPosValid(width,0));
        assertFalse(minef.isPosValid(0,height));
        assertFalse(minef.isPosValid(width +1,0));
        assertFalse(minef.isPosValid(0,height +1));
    }


    @Test
    public void isMine() {
        isMineHelper(minef1, bombs1, height1, width1);
        isMineHelper(minef2, bombs2, height2, width2);

    }
    private void isMineHelper(Minefield mf , List<Position> bombs,
                              int height, int width){
        //Every bomb is recognized.
        for (Position bomb: bombs) {
            assertTrue(mf.isMine(bomb.getX(), bomb.getY()));
        }
        //Every safe tile is recognized
        for (int i=0; i < width; i++){
            for (int j = 0; j< height; j++){
                boolean isSafe = true;
                for (Position b : bombs){
                    if (b.getX() == i && b.getY() == j){
                        isSafe = false;
                    }
                }
                if(isSafe){
                    //ie.: every safe tile is not a mine.
                    assertFalse(mf.isMine(i,j));
                }

            }
        }
    }


}
