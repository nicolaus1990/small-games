import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import java.awt.*;
import java.util.List;

/**
 * Created by nikki on 2/27/18.
 */
public class MinesweeperUISwing {

    private JFrame _frame;
    private JButton[][] _buttons;
    private static final Insets NO_MARGIN = new Insets(0, 0, 0, 0);
    private static final Dimension SQUARE = new Dimension(32, 32);
    private static final Color NORM_COLOR = new ColorUIResource(238, 238, 238);
    private static final Color OPEN_COLOR = new Color(200, 200, 255);

    private int _yAxis;
    private int _xAxis;

    public MinesweeperUISwing(int xAxis, int yAxis){
        _yAxis = yAxis;
        _xAxis = xAxis;
        _frame = new JFrame("Minesweeper");
        _frame.setLayout(new GridLayout(_yAxis, _xAxis));
        _buttons = new JButton[xAxis][yAxis];

        initButtons();

        _frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        _frame.setBounds(32, 0, 1, 1);
        _frame.pack();
        _frame.setResizable(false);
        _frame.setVisible(true);

    }

    private void initButtons() {

        for (int y = 0; y < _yAxis; ++y)
        {
            for (int x = 0; x < _xAxis; ++x)
            {
                JButton button = new JButton();
                button.setBackground(NORM_COLOR);
                button.setMargin(NO_MARGIN);
                button.setPreferredSize(SQUARE);
                _buttons[x][y] = button;
                _frame.add(button);
            }
        }
    }

    public JButton getButton(int x, int y){
        return _buttons[x][y];
    }


    public void gameOverAndWon() {
        JOptionPane.showMessageDialog(null, "You won!",
                "Well done!", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
    public void gameOverAndLost(List<Position> mines){

        for (Position mine : mines) {
            getButton(mine.getX(), mine.getY()).setText("\u0489");
        }
        JOptionPane.showMessageDialog(null,
                "YOU LOSE!", "GAME OVER",
                JOptionPane.ERROR_MESSAGE);
        System.exit(0);

    }
}
