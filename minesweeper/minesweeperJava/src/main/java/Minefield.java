import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by nikki on 2/27/18.
 */
public class Minefield {

    private Tile[][] _minefield;
    private int _yAxis;
    private int _xAxis;
    private int _numTilesToUncover;
    private int _numMines;

    public Minefield (int width, int height, int numMines) {

        _yAxis = height;
        _xAxis = width;
        _numMines = numMines;
        _numTilesToUncover = _yAxis * _xAxis - _numMines;
        initMinefield();
    }


    private Tile getTile(int x, int y){
        return _minefield[x][y];
    }

    private int numAdjacentMines(int x, int y){
        assert(! getTile(x,y).isBomb());
        return getTile(x,y).bombsAround();
    }

    public int getNumAdjMines(Position pos){
        return getTile(pos.getX(),pos.getY()).bombsAround();
    }

    public List<Position> getPosOfBombs(){
        List<Position> ans = new ArrayList<Position>();
        for (int y = 0; y < _yAxis; ++y) {
            for (int x = 0; x < _xAxis; ++x) {
                if (isMine(x, y))
                    ans.add(Position.makePosition(x,y));
            }
        }
        return ans;
    }

    public boolean isMine(int x, int y) {
//        assert(isPosValid(x , y));

        return isPosValid(x,y) && getTile(x, y).isBomb();

    }

    public boolean isPosValid(int x, int y) {
        return y >= 0 && y < _yAxis && x >= 0 && x < _xAxis;
    }

    private void uncoverTile(int x, int y){
        assert(isPosValid(x,y) && !getTile(x,y).isBomb());
        getTile(x,y).uncover();
        --_numTilesToUncover;
    }

    private void uncoverTilesHelper(int x, int y, List<Position> poss) {

        //If tile is not yet explored (i.e.: was not yet uncovered), is a valid position, and is not a bomb.
        if (isPosValid(x, y) && getTile(x,y).isCovered()
                && !getTile(x,y).isBomb()){

            uncoverTile(x,y);
            poss.add(Position.makePosition(x,y));

            int bombsAround = numAdjacentMines(x, y);
            if (bombsAround == 0) {
                uncoverTilesHelper(x - 1, y, poss);
                uncoverTilesHelper(x + 1, y, poss);
                uncoverTilesHelper(x, y - 1, poss);
                uncoverTilesHelper(x, y + 1, poss);
                uncoverTilesHelper(x - 1, y -1, poss);
                uncoverTilesHelper(x - 1, y + 1, poss);
                uncoverTilesHelper(x + 1, y - 1, poss);
                uncoverTilesHelper(x + 1, y + 1 , poss);
            }
        }
    }

    public List<Position> uncoverTiles(int x, int y) {
        List<Position> poss = new ArrayList<Position>();
        uncoverTilesHelper(x,y, poss);
        return poss;
    }

    public boolean gameWon(){
        return _numTilesToUncover == 0;
    }

    private void initMinefield() {
        _minefield = new Tile[_xAxis][_yAxis];
        for (int i = 0; i< _xAxis; i++) {
            for (int j = 0; j < _yAxis; j++) {
                _minefield[i][j] = Tile.setTile(0);
            }
        }

        Random rng = new Random();
        for (int i = 0; i < _numMines; ++i) {
            int y = rng.nextInt(_yAxis);
            int x = rng.nextInt(_xAxis);
            _minefield[x][y] = Tile.setBomb();
//            System.out.println("Bomb: " + x + "," + y);
        }

        for (int i = 0; i< _xAxis; i++) {
            for (int j = 0; j< _yAxis; j++) {
                if (!getTile(i,j).isBomb()) {
                    int numAdjMines = 0;
                    if (isMine(i - 1, j - 1)) ++numAdjMines;
                    if (isMine(i - 1, j)) ++numAdjMines;
                    if (isMine(i - 1, j + 1)) ++numAdjMines;
                    if (isMine(i, j - 1)) ++numAdjMines;
                    if (isMine(i, j + 1)) ++numAdjMines;
                    if (isMine(i + 1, j - 1)) ++numAdjMines;
                    if (isMine(i + 1, j)) ++numAdjMines;
                    if (isMine(i + 1, j + 1)) ++numAdjMines;
                    _minefield[i][j] = Tile.setTile(numAdjMines);
//                    if (numAdjMines != 0){
//                        System.out.println("Pos: (" + i + "," + j
//                                + ")  AdjMines: " + numAdjMines  );
//                    }
                }
            }
        }
    }
}
