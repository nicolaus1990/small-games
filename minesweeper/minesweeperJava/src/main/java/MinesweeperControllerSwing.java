import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

/**
 * Created by nikki on 2/27/18.
 */

public class MinesweeperControllerSwing {


    private MinesweeperUISwing _ui;
    private Minefield _minefield;

    public MinesweeperControllerSwing(int x_Axis, int y_Axis, int numMines) {

        _minefield = new Minefield(x_Axis, y_Axis, numMines);
        _ui = new MinesweeperUISwing(x_Axis, y_Axis);
        addMouseListeners(x_Axis, y_Axis);
    }

    private void addMouseListeners(int xAxis, int yAxis){

        for (int y = 0; y < yAxis; ++y) {
            for (int x = 0; x < xAxis; ++x) {
                JButton button = _ui.getButton(x, y);
                addMouseListener(button, x, y);
            }
        }
    }

    private void addMouseListener(JButton button, int X, int Y){
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent event) {
                if (SwingUtilities.isLeftMouseButton(event)) {
                    if (_minefield.isMine(X, Y)) {
                        _ui.gameOverAndLost(_minefield.getPosOfBombs());
                    }
                    else {
                        if (button.getText().isEmpty()) {
                            for (MouseListener listener : button.getMouseListeners()) {
                                button.removeMouseListener(listener);
                            }
                        }
                        List<Position> poss = _minefield.uncoverTiles(X,Y);
                        updateButtons(poss);
                        if (_minefield.gameWon()){
                            _ui.gameOverAndWon();
                        }
                    }
                }
            }
        });
    }

    private void updateButtons (List<Position> poss){

        for (Position pos : poss) {

            int adjMines = _minefield.getNumAdjMines(pos);
            JButton button = _ui.getButton(pos.getX(),pos.getY());

            if (adjMines == 0) {
                button.setText(" ");
            } else {
                button.setText(String.valueOf(adjMines));
            }
            button.setBackground(new Color(200, 200, 255));
        }
    }
}
