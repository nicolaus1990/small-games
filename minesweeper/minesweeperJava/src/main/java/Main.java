/**
 * Created by nikki on 2/27/
 * Inspired by Fred Winkler's MainzVipa from SE2 SS15
 */
import jdk.nashorn.internal.AssertsEnabled;

import javafx.application.Application;



import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class Main
{
    private static final boolean javafx = true;
    private static final int SIZE_X = 28;
    private static final int SIZE_Y = 18;
    private static final int MINES = 60;


    public Main()
    {

    }


    public static void main(String[] args) {
        if (!AssertsEnabled.assertsEnabled())
            throw new IllegalStateException(
                    "Asserts are disabled! \n" +
                            "Eclipse: Window > Preferences > Java > Installed JREs -> choose JRE " +
                            "click on edit > in Dialog window, Default VM Arguments type -ea. " +
                            "Idea: Click on 'Edit run configurations' and type '-ea' in VM options. ");

        if (javafx) {
            runJavaFx(args);
        } else {
            runSwing();
        }
    }

    private static void runJavaFx(String[] args) {
        Application.launch(MinesweeperUIJavaFX.class, args);
    }

    public static void runSwing(){

        //final GUITool minesweeper = erzeugeKinoMitBeispieldaten();
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                try {
                    UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                new MinesweeperControllerSwing(SIZE_X,SIZE_Y, MINES);
            }
        });
    }


}