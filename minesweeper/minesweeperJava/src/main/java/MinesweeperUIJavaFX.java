import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.List;

public class MinesweeperUIJavaFX extends Application {

    private Button[][] _buttons;
    private int _buttonSize = 35;
    //Images taken from: https://github.com/pardahlman/minesweeper

    int xAxis = 8;
    int yAxis = 10;
    int numMines = 15;
    private Minefield _minefield;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        initModel();
        initGUI(primaryStage);
    }

    private void initGUI(Stage stage) {
        _buttons = new Button[xAxis][yAxis];
        GridPane gp = new GridPane();
        for (int i=0; i < xAxis; i++){
            for (int j=0; j < yAxis; j++){
                Button button = new Button("");
//                button.setStyle(borderStyle);
//                button.setMinSize(_buttonSize, _buttonSize);
                button.getStyleClass().add("button1");

                int finalI = i;
                int finalJ = j;
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        buttonClicked(button, finalI, finalJ);
                    }
                });
                gp.add(button, i, j);
                GridPane.setMargin(button, new Insets(2));
                gp.setAlignment(Pos.CENTER);
                _buttons[i][j] = button;
            }
        }

        BorderPane bp = new BorderPane();
        bp.setPrefSize(700,700);
//        sp.getChildren().add(gp);
        //StackPane.setAlignment(gp,Pos.CENTER_LEFT); //set it to the Center Left(by default it's on the center)
        bp.setCenter(gp);
        stage.setTitle("Minesweeper in FX GUI");
        Scene scene = new Scene(bp,
                xAxis * _buttonSize + 10,
                yAxis * _buttonSize + 10);
        scene.getStylesheets().add("minesweeper.css");
        stage.setScene(scene);

        stage.show();
    }

    private void buttonClicked(Button button, int X, int Y){
        if (_minefield.isMine(X, Y)) {
            gameOverAndLost(_minefield.getPosOfBombs());
        }
        else {
            if (button.getText().isEmpty()) {
                //Unregister Event
                button.setDisable(true);
            }
            List<Position> poss = _minefield.uncoverTiles(X,Y);
            updateButtons(poss);

            if (_minefield.gameWon()){
                gameOverAndWon();
            }
        }
    }

    private void updateButtons (List<Position> poss){

        for (Position pos : poss) {

            int adjMines = _minefield.getNumAdjMines(pos);
            Button button = getButton(pos.getX(),pos.getY());

            if (adjMines == 0) {
                button.setText(" ");
            } else {
                button.setText(String.valueOf(adjMines));
            }
            //button.getStyleClass().add("buttonClicked");
            button.setId("font-button");
        }
    }

    public void gameOverAndWon() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("You won!");
        alert.setHeaderText("GAME OVER");
        alert.setContentText("Congrats!");

        alert.showAndWait();
        System.exit(0);
    }

    private void gameOverAndLost(List<Position> mines) {
        for (Position mine : mines) {
            getButton(mine.getX(), mine.getY()).setText("\u0489");
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("You lost!");
        alert.setHeaderText("GAME OVER");
        alert.setContentText("Better luck next time!");
        alert.showAndWait();

        System.exit(0);
    }

    public Button getButton(int x, int y){
        System.out.println(x + " " + y);
        return _buttons[x][y];
    }


    private void initModel() {
        _minefield = new Minefield(xAxis, yAxis, numMines);
    }
}
