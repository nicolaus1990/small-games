/**
 * Created by nikki on 2/28/18.
 */
public class Tile {

    private boolean _covered;
    private final int _adjBombs;
    private int _bomb = -1;

    private Tile(boolean covered, int adjBombs){
        _covered = true;
        _adjBombs= adjBombs;
    }

    public static Tile setTile(int aB) {
        return new Tile (true, aB);
    }

    public static Tile setBomb(){
        return new Tile(true, -1);
    }

    public boolean isBomb(){return _adjBombs == -1;}
    public boolean isCovered(){ return _covered;}
    public int bombsAround(){return _adjBombs;}
    public void uncover(){ _covered = false;}
}

//    public enum Tile {
//        BOMB, FLAG_AND_BOMB, FLAG_AND_NUM,
//        ZERO,ONE,TWO,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT
//    }