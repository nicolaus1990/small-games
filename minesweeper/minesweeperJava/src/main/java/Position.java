/**
 * Created by nikki on 2/28/18.
 */
public final class Position {
    private int _x;
    private int _y;

    private Position(int x, int y){
        _x = x;
        _y = y;
    }

    public static Position makePosition(int x, int y){
        return new Position(x,y);
    }

    public int getX(){
        return _x;
    }
    public int getY(){
        return _y;
    }
}
