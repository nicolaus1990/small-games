#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode Tkinter tutorial 

This is a simple Nibbles game
clone.

author: Jan Bodnar
website: zetcode.com 
last edited: January 2011

Ported to python3 by Nikki
"""


import sys
import random
from tkinter import Tk, Frame, Canvas, ALL, NW, PhotoImage

DOT_WIDTH = 75
DOT_HEIGHT = 50
DOT_SIZE = 10
WIDTH = DOT_WIDTH * DOT_SIZE
HEIGHT = DOT_HEIGHT * DOT_SIZE
DELAY = 100
ALL_DOTS = WIDTH * HEIGHT / (DOT_SIZE * DOT_SIZE) #Or DOT_WITH * DOT_HEIGHT
#RAND_POS = 27


class SnakeControl:
    def __init__(self,up,down,right,left):
        self.up = up
        self.down = down
        self.right = right
        self.left = left
    def getUp(self):
        return self.up
    def getDown(self):
        return self.down
    def getRight(self):
        return self.right
    def getLeft(self):
        return self.left

class Snake:
    def __init__(self, tag, image, \
                 keys=SnakeControl("Up","Down","Right","Left"),\
                 length=3):
        self.up=False
        self.right=True
        self.down=False
        self.left=False
        self.inGame = True
        self.dots = length
        self.tag = tag
        self.image = image
        self.upKey = keys.getUp()
        self.downKey = keys.getDown()
        self.rightKey = keys.getRight()
        self.leftKey = keys.getLeft()
        self.alive = True

    def movingUp(self):
        return self.up
    def movingRight(self):
        return self.right
    def movingDown(self):
        return self.down
    def movingLeft(self):
        return self.left
    
    def getTag(self):
        return self.tag
    def getImg(self):
        return self.image
    def getDotsTag(self):
        return self.getTag() + "dots"
    def getInGame(self):
        return self.inGame
    def setInGame(self,val):
        self.inGame = val
    def isDead(self):
        return not self.alive
    def declareDead(self,val):
        self.alive = not val
        

    def keyPressed(self, key):
        if key == self.leftKey and not self.movingRight(): 
            self.up = False
            self.down = False
            self.left = True
        elif key == self.rightKey and not self.movingLeft():
            self.up = False
            self.down = False
            self.right = True
        elif key == self.upKey and not self.movingDown():
            self.right = False
            self.left = False
            self.up = True
        elif key == self.downKey and not self.movingUp():
            self.left = False
            self.right = False
            self.down = True



class Board(Canvas):
    
    def __init__(self, parent):
        Canvas.__init__(self, width=WIDTH, height=HEIGHT, 
            background="black", highlightthickness=0)
         
        self.parent = parent 
        self.initGame()
        self.pack()
    
    def initGame(self):

        self.apple_x = 100
        self.apple_y = 190
        
        try:
            img1 = PhotoImage(file="snake01.png")
            img2 = PhotoImage(file="snake02.png")
            self.apple = PhotoImage(file="apple.png")

        except IOError as e:
            print(e)
            sys.exit(1)

        self.snakes = [Snake("head1",img1), \
                       Snake("head2", img2,  SnakeControl("w","s","d","a"))
        ]

        self.focus_get()

        self.createObjects()
        self.locateApple()
        self.bind_all("<Key>", self.onKeyPressed)
        self.after(DELAY, self.onTimer)
        

    def createObjects(self):
    
        self.create_image(self.apple_x, self.apple_y, image=self.apple,\
                          anchor=NW, tag="apple")

        for snake in self.snakes:
            self.create_image(50, 50, image=snake.getImg(), anchor=NW, \
                              tag=snake.getTag())
            self.create_image(30, 50, image=snake.getImg(), anchor=NW, \
                              tag=snake.getDotsTag())
            self.create_image(40, 50, image=snake.getImg(), anchor=NW, \
                              tag=snake.getDotsTag())
   

    def checkApple(self):
        apple = self.find_withtag("apple")
        for snake in self.snakes:
            head = self.find_withtag(snake.getTag())
        
            x1, y1, x2, y2 = self.bbox(head)
            overlap = self.find_overlapping(x1, y1, x2, y2)
            
            for ovr in overlap:
                if apple[0] == ovr:
                    x, y = self.coords(apple)
                    self.create_image(x, y, image=snake.getImg(), anchor=NW, \
                                      tag=snake.getDotsTag())
                    self.locateApple()
        
    
    def doMove(self):

        for snake in self.snakes:
            dots = self.find_withtag(snake.getDotsTag())
            head = self.find_withtag(snake.getTag())
            items = dots + head
        
            z = 0
            while z < len(items)-1:
                c1 = self.coords(items[z])
                c2 = self.coords(items[z+1])
                self.move(items[z], c2[0]-c1[0], c2[1]-c1[1])
                z += 1

            if snake.movingLeft():
                self.move(head, -DOT_SIZE, 0)
            elif snake.movingRight(): 
                self.move(head, DOT_SIZE, 0)
            elif snake.movingUp():
                self.move(head, 0, -DOT_SIZE)
            elif snake.movingDown():
                self.move(head, 0, DOT_SIZE)
            

    def checkCollisions(self):

        for snake in self.snakes:
            dots = self.find_withtag(snake.getDotsTag())
            head = self.find_withtag(snake.getTag())
        
            x1, y1, x2, y2 = self.bbox(head)
            overlap = self.find_overlapping(x1, y1, x2, y2)

            for dot in dots:
                for over in overlap:
                    if over == dot:
                        snake.declareDead(True)

            #Snake outside boundaries?
            if x1 < 0 or \
               x1 > WIDTH - DOT_SIZE or \
               y1 < 0 or \
               y1 > HEIGHT - DOT_SIZE:
                snake.setInGame(False)
            else:
                snake.setInGame(True)
                
    def locateApple(self):
    
        apple = self.find_withtag("apple")
        self.delete(apple[0])
    
        r = random.randint(0, DOT_WIDTH)
        print( "Manzana: valor x:" )
        print( r)
        self.apple_x = r * DOT_SIZE
        r = random.randint(0, DOT_HEIGHT)
        print( "Manzana: valor y: " )
        print( r)
        self.apple_y = r * DOT_SIZE
        
        self.create_image(self.apple_x, self.apple_y, anchor=NW,
            image=self.apple, tag="apple")
                

    def onKeyPressed(self, e): 
    
        key = e.keysym
        for snake in self.snakes:
            snake.keyPressed(key)

    def onTimer(self):

        if any(map(lambda snake: snake.getInGame(), self.snakes)) and \
           all(map(lambda snake: not snake.isDead(), self.snakes)):
            self.checkCollisions()
            self.checkApple()
            self.doMove()
            self.after(DELAY, self.onTimer)
        else:
            self.gameOver()
            
             
    def gameOver(self):

        self.delete(ALL)
        self.create_text(self.winfo_width()/2, self.winfo_height()/2, 
            text="Game Over", fill="white")


class Nibbles(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)
        parent.title('Nibbles')
        self.board = Board(parent)
        self.pack()


def main():

    root = Tk()
    nib = Nibbles(root)
    root.mainloop()  


if __name__ == '__main__':
    main()
