(ns snake-cljs.core
    (:require [snake-cljs.tools :refer 
               [has? repeatStr resetVal! 
                randomHexColor insertAfter log error]]
              ))

(enable-console-print!)

(println "This text is printed from src/snake-cljs/core.cljs. Go ahead and edit it and see reloading in action.")

;; define your app data so that it doesn't get over-written on reload
;;(defonce app-state (atom {:text "Hello world!"}))


(def grid 16)
(def canvasWidth 400)
(def canvasHeight 400)

(def loopCount (atom 0))
(def context (atom nil))
(def apple (atom {:x 320 :y 320}))
(def appleColor "red")
(def snakesInitialLength 4)
(def bodyColor (rand-nth ["blue" "green" "yellow"]))
(def headColor (randomHexColor))

(def beginningSnake 
  {:x 160 :y 160 ;;Head
   :dx grid :dy 0;;Direction
   :body []
   :length 4;;Length of snake
   })
(def snake (atom beginningSnake))

(defn resetSnake! []
  (reset! snake beginningSnake))
(defn getHead 
  ([] {:x (:x @snake) :y (:y @snake)})
  ([coord] 
   (case coord
     :x (:x @snake)
     :y (:y @snake)
     (error (str "Wrong use of getHead. This was given: " 
                 coord)))))
(defn getHeadX [] (:x @snake))
(defn getHeadY [] (:y @snake))
(defn getBody [] (:body @snake))
(defn getBodyLength [] (:length @snake))
(defn snakesHeadAndBody []
  (conj (getBody) (getHead)))

(defn snake-change-dir! [dx dy]
  (swap! snake update-in [:dx] (fn [old new] new) dx)
  (swap! snake update-in [:dy] (fn [old new] new) dy)
  )


(defn moveSnake! []
  ;; Returns false if apple was not eaten.
  ;;Update snakes body 
  ;; i)  Remove last cell of body (if no apple was eaten.
  ;; ii) Add cell which was previously the head
  (let [appleEaten  (has? (snakesHeadAndBody) @apple)]
    ;;Increment length if apple was eaten.
    (swap! snake update-in [:length] (if appleEaten inc identity))
    ;;Only remove last cell of body, if snake is big enough (in the
    ;;beginning it's only the head in datastructure).
    (when (not (< snakesInitialLength (count (getBody)))) (log @snake))
    (when (< snakesInitialLength (count (getBody)))
      (resetVal! snake [:body] (take ((if appleEaten inc identity) (getBodyLength))
                                     (getBody))))
    ;;Add to body what was previously the head 
    (swap! snake update-in [:body] conj 
           (getHead))
    ;; Update snakes head 
    (swap! snake update-in [:x] + (:dx @snake))
    (swap! snake update-in [:y] + (:dy @snake))
    ;; Wrap snake position horizontally/vertically on edge of screen
    (cond (< (getHead :x) 0) 
          (resetVal! snake [:x] (- canvasWidth grid))
          (< canvasWidth (getHead :x))
          (resetVal! snake [:x] 0))
    (cond (< (getHead :y) 0) 
          (resetVal! snake [:y] (- canvasHeight grid))
          (< canvasHeight (getHead :y))
          (resetVal! snake [:y] 0))
    ;;Check if snake bit its tail
    (when (has? (getBody) (getHead))
      (resetSnake!))
    appleEaten))

(defn drawSnake! []
  ;;Draws snake
  (do ;;Draw head
    (set! (.-fillStyle @context) headColor)
    ;;drawing 1 px smaller than the grid creates a grid effect in the
    ;;snake body so you can see how long it is
    (.fillRect @context 
               (getHead :x) (getHead :y)
               (dec grid) (dec grid))
    (set! (.-fillStyle @context) bodyColor)
    ;;Draw body
    (doseq [cell (getBody)]
      (.fillRect @context 
                 (:x cell) (:y cell)
                 (dec grid) (dec grid)))))


(defn drawApple! [appleEaten]
  (when appleEaten 
    ;;canvas is 400x400 which is 25x25 grids 
    (resetVal! apple [:x] (* (rand-int 25) grid))
    (resetVal! apple [:y] (* (rand-int 25) grid)))
  (do (set! (.-fillStyle @context) appleColor)
      (.fillRect @context 
                 (:x @apple) (:y @apple)
                 (dec grid) (dec grid))))


(defn gameLoop []
  (.requestAnimationFrame js/window gameLoop)
  (swap! loopCount inc)
  ;;slow game loop to 15 fps instead of 60 (60/15 = 4)
  (when (< 4 @loopCount)
    (reset! loopCount 0)
    (.clearRect @context 0 0 canvasWidth canvasHeight)
    (drawApple! (moveSnake!))
    (drawSnake!)))

(defn snakeWantsToChangeDir! [event]
  ;;prevent snake from backtracking on itself by checking that it's
  ;;not already moving on the same axis (pressing left while moving
  ;;left won't do anything, and pressing right while moving left
  ;;shouldn't let you collide with your own body
  ;; left, up, right, down := 37-40 respectively
  (cond (and (= (:dx @snake) 0) 
             (=  (.-which event) 37))
        (snake-change-dir! (- grid) 0)
        (and (= (:dy @snake) 0) 
             (=  (.-which event) 38))
        (snake-change-dir! 0 (- grid))
        (and (= (:dx @snake) 0) 
             (=  (.-which event) 39))
        (snake-change-dir! grid 0)
        (and (= (:dy @snake) 0) 
             (=  (.-which event) 40))
        (snake-change-dir! 0 grid)))

(defn startGame [node] 
  (let [canvas (.createElement js/document "canvas")]
    (.setAttribute canvas "width" (str canvasWidth))
    (.setAttribute canvas "height" (str canvasHeight))
    (.setAttribute canvas "id" "snakeCanvas")
    (.appendChild node canvas)
    ;; In js: var context = canvas.getContext('2d');
    (reset! context (.getContext canvas "2d"))
    ;; Add key event listeners
    (.addEventListener js/document "keydown" 
                       snakeWantsToChangeDir!)
    ;;Start animation
    (js/requestAnimationFrame gameLoop)))

(defn startup []
  (let [;;New nodes to insert
        description "This game was taken (and ported to cljs) from Steven Lambert's gist: "
        url "https://gist.github.com/straker/ff00b4b49669ad3dec890306d348adc4"
        paragraph (-> js/document (.createElement "p"))
        credits (-> js/document (.createElement "a"))
        pathToCSS "static/index-page/snake-cljs/snake_cljs_style.css"
        cssLink (-> js/document (.createElement "link"))
        gameDiv (-> js/document (.createElement "div"))
        ;;Nodes to insert to:
        div (-> js/document (.getElementById "mainDiv"))
        head (-> js/document 
                 (.getElementsByTagName "head")
                 ;; in js it would be: [0] (get first elem of array)
                 array-seq first)]
    (.setAttribute cssLink "href" pathToCSS)
    (.setAttribute cssLink "type" "text/css")
    (.setAttribute cssLink "rel" "stylesheet")
    (.setAttribute cssLink "media" "screen,print")
    (.setAttribute credits "href" url)
    (.setAttribute gameDiv "id" "gameDiv")
    (set! (.-innerHTML credits) "click here.")
    (set! (.-innerHTML paragraph) description)
    (.appendChild paragraph credits)
    ;;Insert css link
    (.appendChild head cssLink)
    ;;Insert description
    (.appendChild div paragraph)
    ;;Insert gameDiv
    (.appendChild div gameDiv)
    ;;Insert game
    (startGame gameDiv)))

(js/setTimeout startup 1000)


(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
