(ns snake-cljs.tools
    (:require [clojure.string :as cljStr]
              [clojure.set :as cljSet]))


(defn any-true? [xs] 
  ;;Never use 'any?' (because it always returns true).
  (some identity xs))

(defn all-true [xs]
  (apply = true xs))

(defn repeatStr [someStr numtimes]
  (apply str (repeat numtimes someStr)))

(defn has? [col elem]
  ;;Given a collection, check if it has elem.
  (some #(= % elem) col))

;;;;;;;;;;;;;;;;;;;
;;; State management
;;;;;;;;;;;;;;;;;;;

(defn resetVal! [someAtom dirs newVal]
  ;;Updates some deeply nested elem in atom 'someAtom', 
  ;;regardless of previous value.
  (swap! someAtom update-in dirs (fn [old new] new) newVal))

;;;;;;;;;;;;;;;;;;;
;;; RBG Color javascript
;;;;;;;;;;;;;;;;;;;

(defn randomHexColor []
  ;;Taken from: https://gist.github.com/martinklepsch/8730542
  ;; These would have also worked:
  ;;(str "#" (clojure.string/join (repeatedly 6 #(rand-nth c))))
  ;;(.toString (rand-int 16rFFFFFF) 16)
  (let [c [0 1 2 3 4 5 6 7 8 9 \A \B \C \D \E \F]]
    (apply str \# (repeatedly 6 #(rand-nth c)))))

;;;;;;;;;;;;;;;;;;;
;;; DOM utlities
;;;;;;;;;;;;;;;;;;;

(defn insertAfter [newNode referenceNode]
  ;; in js: referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  (.. referenceNode -parentNode 
      (insertBefore newNode 
                     (.-nextSibling referenceNode))))

;;;;;;;;;;;;;;;;;;;
;;; Browser logging and error messages
;;;;;;;;;;;;;;;;;;;

(defn log [& txts] (.log js/console (apply str txts)))
(defn log-ind [indentation & txts] 
  ;;Log txt with indentation. (Useful fn for recursion).
  (.log js/console (str (repeatStr "     " indentation) (apply str txts))))
(defn error [& txts] (js/alert (apply str txts)));;TODO!
(defn alert [& txts] (js/alert (apply str txts)))
