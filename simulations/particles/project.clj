(defproject particles "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  ;; :license {:name "Eclipse Public License"
  ;;           :url "http://www.eclipse.org/legal/epl-v10.html"}
  :source-paths ["src/clj" "src/cljs"]
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.520"]]
  :plugins [[lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]]
  :cljsbuild {:builds
              [
               ;; {
               ;;  :source-paths ["src/cljs"]
               ;;  :compiler {
               ;;             :output-to "resources/public/js/app.js"
               ;;             :optimizations :whitespace
               ;;             :pretty-print true}}
               ;;
               {:id "min"
                :source-paths ["src/cljs"]
                :compiler {:output-to "resources/public/js/simulation_particles_cljs.js"
                           :main particles.core
                           :optimizations :advanced
                           :pretty-print false}}]}
  ;;To use only alias: 'lein build'
  :aliases {"build" ["cljsbuild" "once" "min"]})

