(ns particles.core)

;(def display (.getElementById js/document "display"))
;(def context (.getContext display "2d"))
(def display (atom nil))
(def context (atom nil))

(def damping 0.999)
(def max-particles 200)
(def line-width 2)

;; (def app-state (atom {:width (.-innerWidth js/window)
;;                       :height (.-innerHeight js/window)}))

(def app-state (atom nil))

(defn init-canvas 
  []
  (let [{:keys [width height]} @app-state]
    (do
      (set! (.-width @display) width))
      (set! (.-height @display) height)))

(defn rand-rgb
  []
  (let [r (rand-int 255)
        g (rand-int 255)
        b (rand-int 255)]
    (str "rgb(" r "," g "," b ")")))

(defn create-particle
  []
  (let [width (:width @app-state)
        height (:height @app-state)
        x (rand width)
        y (rand height)
        color (rand-rgb)]
    {:x x, :y y, :old-x x, :old-y y, :color color}))
                      
;; (swap! app-state assoc :mouse {:x (* (:width @app-state) 0.5) 
;;                                :y (* (:height @app-state) 0.5)})
;; (swap! app-state assoc :particles (vec (map create-particle (range max-particles))))


(defn integrate
  [{:keys [x y old-x old-y] :as particle}]
  (let [velocity-x (* (- x old-x) damping)
        velocity-y (* (- y old-y) damping)]
    (assoc particle :x (+ x velocity-x)
                    :y (+ y velocity-y)
                    :old-x x
                    :old-y y)))

(defn attract
  [pos-x pos-y {:keys [x y] :as particle}]
  (let [dx (- pos-x x)
        dy (- pos-y y)
        distance (.sqrt js/Math (+ (* dx dx) (* dy dy)))
        new-x (+ x (/ dx distance))
        new-y (+ y (/ dy distance))]
    (assoc particle :x new-x :y new-y)))

(defn draw
  [ctx {:keys [x y old-x old-y color]}]
  (do
    (set! (.-strokeStyle ctx) color)
    (set! (.-lineWidth ctx) line-width)
    (.beginPath ctx)
    (.moveTo ctx old-x old-y)
    (.lineTo ctx x y)
    (.stroke ctx)))


(defn render
  []
  (.requestAnimationFrame js/window render)
  (let [{:keys [width height particles mouse]} @app-state]
    (.clearRect @context 0 0 width height)
    (dotimes [n max-particles]
      (let [particle (nth particles n)
            x (:x mouse)
            y (:y mouse)
            p (integrate (attract x y particle))]
        (draw @context p)
        (swap! app-state assoc-in [:particles n] p)))))

(defn mousemove-handler
  [event]
  (swap! app-state assoc :mouse {:x (.-clientX event)
                                 :y (.-clientY event)})
  (swap! app-state assoc :mouse {:x (- (.-clientX event) 400)
                                 :y (.-clientY event)})
)

(defn windowresize-handler
  [event]
  (let [w (.-innerWidth js/window)
        h (.-innerHeight js/window)]
    (do
      (swap! app-state assoc :width w)
      (swap! app-state assoc :height h)
      (init-canvas))))

;; (.addEventListener display "mousemove" mousemove-handler)
;; (.addEventListener js/window "resize" windowresize-handler)

;; (init-canvas)
;; (render)








;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;; END OF ChillyBwoy/core.cljs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def canvasWidth 400)
(def canvasHeight 400)

(defn chillyBwoysCodeInAFn [canvas]
  (reset! display canvas)
  (reset! app-state {:width canvasWidth
                     :height canvasHeight})
  (swap! app-state assoc :mouse {:x (* (:width @app-state) 0.5) 
                                 :y (* (:height @app-state) 0.5)})
  (swap! app-state assoc :particles (vec (map create-particle (range max-particles))))
  (.addEventListener @display "mousemove" mousemove-handler)
  (.addEventListener js/window "resize" windowresize-handler)
  (init-canvas)
  (render)
)



(defn startGame [node] 
  (let [canvas (.createElement js/document "canvas")]
    (.setAttribute canvas "width" (str canvasWidth))
    (.setAttribute canvas "height" (str canvasHeight))
    (.setAttribute canvas "id" "display")
    (.appendChild node canvas)
    ;; In js: var context = canvas.getContext('2d');
    (reset! context (.getContext canvas "2d"))
    ;;Start animation
    (chillyBwoysCodeInAFn canvas)))

(defn startup []
  ;; The div to retrive (inside html file) has id "mainDiv"
  (let [pathToCSS "static/index-page/simulation-particles/simulation_particles_cljs_style.css"
        description "This simulation was taken as it was from  ChillyBwoy gist: "
        url "https://gist.github.com/ChillyBwoy/38741e0005826fc84e9e"
        ;;New nodes to insert
        paragraph (-> js/document (.createElement "p"))
        credits (-> js/document (.createElement "a"))
        cssLink (-> js/document (.createElement "link"))
        gameDiv (-> js/document (.createElement "div"))
        ;;Nodes to insert to:
        div (-> js/document (.getElementById "mainDiv"))
        head (-> js/document 
                 (.getElementsByTagName "head")
                 ;; in js it would be: [0] (get first elem of array)
                 array-seq first)]
    (.setAttribute cssLink "href" pathToCSS)
    (.setAttribute cssLink "type" "text/css")
    (.setAttribute cssLink "rel" "stylesheet")
    (.setAttribute cssLink "media" "screen,print")
    (.setAttribute credits "href" url)
    (.setAttribute gameDiv "id" "gameDiv")
    (set! (.-innerHTML credits) "click here.")
    (set! (.-innerHTML paragraph) description)
    (.appendChild paragraph credits)
    ;;Insert css link
    (.appendChild head cssLink)
    ;;Insert description
    (.appendChild div paragraph)
    ;;Insert gameDiv
    (.appendChild div gameDiv)
    ;;Insert game
    (startGame gameDiv)))

(js/setTimeout startup 1000)
