#lang racket

(provide ;;API structs
         (struct-out pos)
         (struct-out block)
         (struct-out tetra)
         (struct-out player)
         (struct-out game)
         build-serialized-msg
         get-msg-content
         ;;Playing mode
         get-score
         PLAYER-UPDATE
         ALL-PLAYERS-UPDATE
         ;; Waiting mode
         START-REQUEST
         START-GAME
         requestStart?
         id?
         id=?
         keep-waiting-msg?
         waiting-get-progress
         start-game?
         
 )

#| --- Protocol ----------------------------------------------------------------
     Client1        Client2         Server
       |               |              |
       | register(name1)              |   [universe protocol]
       |----------------------------->|
       |               |              |  
       |               |       ID     |    an identifier message 
       |<-----------------------------|
       |               |       t      |    percentage of wait time 
       |<-----------------------------|
       |<-----------------------------|
       |<-----------------------------|
       |               |              |    
       |               | register(name2) 
       |               |------------->|
       |               |              |  
       |               |       ID     |  
       |               |<-------------|
       |               |       t      |    percentage of wait time 
       |<-----------------------------|
       |               |<-------------|
       |<-----------------------------|
       |               |<-------------|
       |               |              |  <==== end of wait time [clock, players]
       |             state msg        |  
       |<-----------------------------|    `(state ???)
       |               |<-------------|
       |               |              |  
 step  |  STEP 	       |	      |    `(step ???)
 ====> |----------------------------->|    new state 
       |               |              |  
       |             state msg        |     
       |<-----------------------------|    `(state (,player1 ,player2))
       |               |<-------------|
       |               |              |  
       |               |              |    move, eat:
       |<-----------------------------|    `(state (,player1 ,player2))
       |               |<-------------|
       |               |              |    
       |        step   | STEP	      |   `(step ???)
       |	====>  |------------->|  
       |               |              |  
       |             state msg        |     
       |<-----------------------------|    `(state (,player1 ,player2))
       |               |<-------------|
       |               |              |
       |             score msg        |    only one left: 
       |<-----------------------------|    `(score ((,id ,score) ...))
       |               |<-------------|
       |               |              |
      ---             ---            ---
|#

;==================Data Definitions=================;

;; Before: poss from htdp-advanced-reader.ss
(define-struct pos (x y) #:prefab)

;; A Block is a (make-block Number Number Color)
(define-struct block (x y color) #:prefab)

;; A Tetra is a (make-tetra pos BSet)
;; The center point is the point around which the tetra rotates
;; when it spins.
(define-struct tetra (center blocks) #:prefab)


;; A [Listof X] is one of:
;; - empty
;; - (cons X [Listof X])

;; A Set of Blocks (BSet) is [Listof Block]
;; Order does not matter.  Repetitions are NOT allowed.



;; -----------------------------------------------------
;; Taken from shared.rkt chapter 14

;; Shared Data Definitions for Messages 

;;;(struct body (size loc) #:prefab #:mutable)
(struct game (tetra pile) #:prefab #:mutable)
(struct player (id [game #:mutable]) #:prefab)

;; Msg functions when waiting for players and loading.

;; PlayerId = String
(define id? string?)
(define id=? string=?)

(define waiting-get-progress (lambda (msg) msg))
(define UPDATE-LENGTH 2)
(define PLAYER-UPDATE 'playerUpdate)
(define ALL-PLAYERS-UPDATE 'playersUpdate)
;; Waiting/Joining state
(define keep-waiting-msg? number?)
(define START-REQUEST 'loadingOrAboutToStart?)
(define START-GAME 'startGame)
(define requestStart? (lambda (s) (eq? START-REQUEST s)))
(define (update-message-generic? msg)
  (and (list? msg)
       (= UPDATE-LENGTH (length msg))
       (symbol? (first msg))
       ;(symbol? (second msg))
       ;(list? (third msg))
       ;(symbol=? SERIALIZE (first msg))
       ;;(symbol=? SERIALIZE (second msg));Second is how the third elem is to be interpreted
       ;;(andmap player? (third msg))
       ))
(define (start-game? msg)
  (and ;(update-message-generic? msg)
       (symbol? (first msg))
       (symbol=? START-GAME (first msg))
       (andmap player? (second msg))
       ))
(define (update-message? msg)
  (and (update-message-generic? msg)
       (symbol=? PLAYER-UPDATE (second msg))
       (andmap player? (third msg))))

(define get-msg-content cadr)

(define (msg-type? t)
  (or (eq? t START-GAME)
      (eq? t PLAYER-UPDATE)
      (eq? t ALL-PLAYERS-UPDATE)))

(define (build-serialized-msg type content)
  (when (not (msg-type? type)) (error (~a "Wrong message type" type)))
  (list type content))



;; Number -> Number ;; move to serer 
;; gets aplayers score given its fatness
(define (get-score f)
  (/ (- f 20) 100))